package com.yusep.helloSpringboot;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@ComponentScan
@RestController
public class Controller {

    @GetMapping("/test")
    public ResponseEntity testingApi (){
        return new ResponseEntity("this is testing nyooy", HttpStatus.OK);
    }
}
